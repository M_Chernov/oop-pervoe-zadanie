package com.company.interfaces;

public interface Methods {
    void addToLibrary();
    String getNameOrAuthor();
    int getNumber();
    void deleteFromLibrary();
    void google();
    void printAll();
}
