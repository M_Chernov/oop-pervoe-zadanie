package com.company.interfaces;

public interface Commands {
    void start();
    void inputCommand();
    void commandParser(String parser);
    void selectLibrary();
    void help();
}
