package com.company.services;


import com.company.interfaces.Commands;

import java.util.Scanner;

public class Libraries implements Commands {
    Library[] libraries = new Library[5];
    int selectLibrary = -1;
    String commands ="select - выбор библиотеки\n" +
            "add - добавить книгу\n" +
            "delete - удалить книгу\n" +
            "find - найти книгу\n" +
            "all - показать все книги";

    public void start(){
        for (int i=0;i<libraries.length;i++)
            libraries[i]=new Library();
        selectLibrary();
        inputCommand();
    }

    public void inputCommand(){
        System.out.println("Введите команду");
        Scanner scanner = new Scanner(System.in);
        String command = scanner.nextLine();
        commandParser(command);
    }

    public void commandParser(String parser){
        switch (parser) {
            case "select": selectLibrary();
            break;
            case "add": libraries[selectLibrary].addToLibrary();
            break;
            case "delete": libraries[selectLibrary].deleteFromLibrary();
            break;
            case "find": libraries[selectLibrary].google();
            break;
            case "all": libraries[selectLibrary].printAll();
            break;
            case "help": help();
            break;
            default:
                System.out.println("введена неверная команда");
        }

        inputCommand();
    }

    public void selectLibrary() {
        System.out.println("введите номер библиотеки");
        Scanner scanner = new Scanner(System.in);
        int select = scanner.nextInt();
        if (select < libraries.length) {
            selectLibrary = select;
        }
        else {
            System.out.println("введите корректный номер библиотеки");
        }
    }

    public void help() {
        System.out.println(commands);
    }
}
