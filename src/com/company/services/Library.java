package com.company.services;

import com.company.interfaces.Methods;

import java.util.Scanner;

public class Library implements Methods {
    Book[] library = new Book[5];
    int numberOfBook = 0;

    public void addToLibrary() {
        if (numberOfBook < library.length) {
            System.out.print("Введите название книги: ");
            String name = getNameOrAuthor();

            System.out.print("Введите автора книги: ");
            String author = getNameOrAuthor();

            System.out.print("Введите год: ");
            int age = getNumber();

            library[numberOfBook] = new Book(name, author, age);
            numberOfBook++;
        }
        else {
            System.out.println("Библиотека заполнена");
        }
    }

    public String getNameOrAuthor() {
        Scanner scanner = new Scanner(System.in);
        String nameOrAuthor = scanner.nextLine();
        return nameOrAuthor;
    }

    public int getNumber() {
        Scanner scanner = new Scanner(System.in);
        int age = Integer.parseInt(scanner.nextLine());
        return age;
    }

    public void deleteFromLibrary() {
        if (numberOfBook > 0) {
            System.out.print("введите индекс удаляемой книги");
            int index = getNumber();
            if (index < numberOfBook) {
                for (int i = index; i < numberOfBook - 1; i++) {
                    library[i] = library[i + 1];
                }
                library[numberOfBook] = null;
                numberOfBook--;
            }
            else {
                System.out.println("введен некорректный индекс");
            }
        }
        else {
            System.out.println("книг нет");
        }
    }

    public void google() {
        System.out.print("Введите название книги: ");
        String name = getNameOrAuthor();
        for (int index = 0; index < numberOfBook; index++) {
            if (library[index].checkBook(name)) {
                System.out.println(index + " " + library[index].nameOfBook + " " + library[index].author + " " + library[index].age);
            }
        }
    }

    public void printAll() {
        for (int index = 0; index < numberOfBook; index++) {
            System.out.println(index + " " + library[index].nameOfBook + " " + library[index].author + " " + library[index].age);
        }
    }
}
