package com.company.services;

public class Book {
    public String nameOfBook, author;
    public int age;

    public Book(String nameOfBook, String author, int age) {
        this.nameOfBook = nameOfBook;
        this.author = author;
        this.age = age;
    }

    public boolean checkBook(String name) {
        if(nameOfBook.equals(name)) {
            return true;
        }
        else {
            return false;
        }
    }
}
